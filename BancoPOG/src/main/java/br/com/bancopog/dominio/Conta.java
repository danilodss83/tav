package br.com.bancopog.dominio;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import br.com.bancopog.persistencia.Dao;

public class Conta {

	private BigDecimal saldo = BigDecimal.ZERO;
	private BigDecimal limite = BigDecimal.ZERO;
	
	private Cliente titular;
	private int numero;
	private Calendar dataAbertura;
	private static int geradorDeNumeros = 10000;
	
	public Conta( Calendar dataAbertura, Cliente titular) {
		this.numero = geraNumeroConta();
		this.dataAbertura = dataAbertura;
		this.titular = titular;
	}
	
	/**
	 * Gera números para novas contas no banco
	 * @return Número da próxima conta
	 */
	
	public int geraNumeroConta() {
		geradorDeNumeros += new Random(42).nextInt(1000);
		return geradorDeNumeros++;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public Calendar getDataAbertura() {
		return dataAbertura;
	}
	
	public BigDecimal getSaldo() {
		return saldo;
	}

	public BigDecimal getLimite() {
		return limite;
	}
	
	public void setLimite(BigDecimal limite) {
		this.limite = limite;
	}
	
	public Cliente getTitular() {
		return titular;
	}
	
	public void setTitular(Cliente titular) {
		this.titular = titular;
	}
	
	@Override
	public String toString() {
		return "{Conta #" + this.numero + "}";
	}
	
	
}
