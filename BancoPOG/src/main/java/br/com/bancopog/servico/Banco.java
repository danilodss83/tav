package br.com.bancopog.servico;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import br.com.bancopog.dominio.Cliente;
import br.com.bancopog.dominio.Conta;
import br.com.bancopog.persistencia.Dao;

public class Banco {	
	/**
	 * Registra um novo cliente no sistema
	 * @param campoNome Recebe valor digitado no formulário com o nome.
	 * @param campoCpf  Recebe valor digitado no formulário com o cpf.
	 * @return Novo cliente persistido no sistema
	 */
	
	public Cliente registraCliente(String nome, String cpf)  throws SQLException {
		// recebe os dados de um formulário, cria um cliente e guarda no banco de dados
			Cliente cliente = criaCliente(nome,cpf);
			Dao<Cliente> dao = new Dao<Cliente>();
			dao.adiciona(cliente);
			
			return cliente;
		
		}
		
	public Cliente criaCliente(String nome, String cpf){
		
		Cliente cliente = new Cliente(nome,cpf);
		
		return cliente;
	}
	
	/**
	 * Registra nova conta no sistema
	 * @param titular Cliente titular da conta. Já deve ter sido aprovado na consulta ao Serasa.
	 * @param numero  Número da conta
	 * @return Nova Conta, aberta e registrada
	 */
	public Conta registraConta(Cliente cliente) throws SQLException {
		
		Conta novaConta = criaConta(cliente);
		Dao<Conta> dao = new Dao<Conta>();
		dao.adiciona(novaConta);
		
		return novaConta;
	}
	
	public Conta criaConta(Cliente titular){
		Calendar hoje = Calendar.getInstance();
		
		Conta novaConta = new Conta(hoje, titular);
		
		return novaConta;
		
	}
}








