package br.com.bancopog.servico;

import java.math.BigDecimal;

import br.com.bancopog.dominio.Conta;

/**
 * Operações necessárias para nosso Banco
 */
public class OperacoesBancarias {

	/**
	 * Saca dinheiro de uma conta bancária
	 * 
	 * @param conta Conta onde sacar
	 * @param valor Valor a ser sacado
	 */
	public void saca(Conta conta, BigDecimal valor) {
		BigDecimal novoSaldo = conta.getSaldo().subtract(valor);
		
		if (novoSaldo.compareTo(conta.getLimite().negate()) >= 0) {
			conta.setSaldo(novoSaldo);
		} else {
			throw new RuntimeException("Saldo insuficiente");
		}
	}
	
	/**
	 * Deposita dinheiro em uma conta bancária
	 * 
	 * @param conta Conta onde depositar
	 * @param valor Valor a ser sacado
	 */
	public void deposita(Conta conta, BigDecimal valor) {
		BigDecimal novoSaldo = conta.getSaldo().add(valor);
		conta.setSaldo(novoSaldo);
	}
}
